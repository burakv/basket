# Basketball Simulation

Run example simulation with `make run`:

```
$ make run
Score: 104 - 87
Stats:
        Home:
                Attack Count: 181
                Player 1: 2/4 3pt, 7/9 2pt, 10 assists
                Player 2: 2/8 3pt, 4/5 2pt, 3 assists
                Player 3: 4/5 3pt, 12/16 2pt, 12 assists
                Player 4: 3/5 3pt, 2/2 2pt, 9 assists
                Player 5: 3/8 3pt, 6/10 2pt, 8 assists
        Away:
                Attack Count: 181
                Player 1: 2/2 3pt, 12/15 2pt, 11 assists
                Player 2: 2/5 3pt, 8/12 2pt, 11 assists
                Player 3: 2/5 3pt, 4/8 2pt, 8 assists
                Player 4: 2/7 3pt, 0/4 2pt, 3 assists
                Player 5: 1/4 3pt, 6/9 2pt, 6 assists
```

## Tests

```
$ make test
```

## Benchmarks

```
$ make bench
```

## Linting

```
$ make lint
```
