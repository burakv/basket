.PHONY: test
test: 
	go test -v -cover ./...

.PHONY: bench
bench: 
	go test -v -bench=. ./...

.PHONY: lint
lint: 
	go vet ./... && \
	golint ./...

.PHONY: run
run: 
	go run cmd/basket/main.go

