package main

import (
	"fmt"
	"math/rand"
	"time"

	"basket/game"
	"basket/game/simulation"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	game := game.NewGame()

	simulation.Simulate(game)

	fmt.Printf("Score: %d - %d\n", game.Home.Score, game.Away.Score)
	fmt.Println("Stats:")
	fmt.Println("\tHome:")
	printTeamStats(game.Home)
	fmt.Println("\tAway:")
	printTeamStats(game.Away)
}

func printTeamStats(team *game.Team) {
	fmt.Printf("\t\tAttack Count: %d\n", team.AttackCount)
	for _, p := range team.Players {
		fmt.Printf("\t\tPlayer %d: %d/%d 3pt, %d/%d 2pt, %d assists\n",
			p.No, p.ThreePointScores, p.ThreePointAttempts,
			p.TwoPointScores, p.TwoPointAttempts, p.Assists)
	}
}
