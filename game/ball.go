package game

// A Ball represents a ball in Basketball.
type Ball struct {
	Owner     *Player
	PrewOwner *Player
	Area      Area
}
