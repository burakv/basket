package game

// A Player represents a player in a Basketball Game.
type Player struct {
	No                 int
	Assists            int
	TwoPointScores     int
	TwoPointAttempts   int
	ThreePointScores   int
	ThreePointAttempts int
}

// NewPlayer returns a new player with given number.
func NewPlayer(no int) *Player {
	return &Player{No: no}
}
