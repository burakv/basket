// Package game provides utilities that represents Basketball Game.
package game

import (
	"time"
)

// Game related durations.
const (
	GameDuration   = 48 * time.Minute
	AttackDuration = 24 * time.Second
)

// Game score types.
const (
	TwoPoint   = 2
	ThreePoint = 3
)

// A Game represents a running Basketball Game.
type Game struct {
	Remaining           time.Duration
	RemainingAttackTime time.Duration
	Ball                *Ball
	Home                *Team
	Away                *Team
}

// NewGame returns a new Game with random teams.
func NewGame() *Game {
	remaining := GameDuration
	remainingAttackTime := AttackDuration
	ball := &Ball{}
	home := NewTeam()
	away := NewTeam()
	return &Game{remaining, remainingAttackTime, ball, home, away}
}

// Tick advances game time with given `d`.
func (g *Game) Tick(d time.Duration) {
	g.Remaining -= d
	g.RemainingAttackTime -= d
}

// End returns whether game is end.
func (g *Game) End() bool {
	return g.Remaining <= 0
}

// ResetAttackDuration sets remaining attack time to default.
func (g *Game) ResetAttackDuration() {
	if g.Ball.Owner != nil {
		g.PlayerTeam(g.Ball.Owner).AttackCount++
	}
	g.RemainingAttackTime = AttackDuration
}

// RemainingAttackDurationRatio return ratio of the remaining attack time to maximum attack time.
func (g *Game) RemainingAttackDurationRatio() float64 {
	return float64(g.RemainingAttackTime) / float64(AttackDuration)
}

// HoldBall changes ball owner to given `player`.
func (g *Game) HoldBall(player *Player) {
	g.Ball.PrewOwner = g.Ball.Owner
	g.Ball.Owner = player
}

// Scored records a `score` from ball owner.
func (g *Game) Scored(score int) {
	owner := g.Ball.Owner
	if owner == nil {
		panic("ball not owned")
	}
	scoredTeam := g.PlayerTeam(owner)
	scoredTeam.Score += score

	if g.Ball.PrewOwner != nil &&
		g.PlayerTeam(g.Ball.PrewOwner) == scoredTeam {
		g.Ball.PrewOwner.Assists++
	}
	if score == TwoPoint {
		g.Ball.Owner.TwoPointScores++
		g.Ball.Owner.TwoPointAttempts++
	} else {
		g.Ball.Owner.ThreePointScores++
		g.Ball.Owner.ThreePointAttempts++
	}
}

// Missed records a `score` attempt from ball owner.
func (g *Game) Missed(score int) {
	owner := g.Ball.Owner
	if owner == nil {
		panic("ball not owned")
	}

	if score == TwoPoint {
		g.Ball.Owner.TwoPointAttempts++
	} else {
		g.Ball.Owner.ThreePointAttempts++
	}
}

// PlayerTeam returns `player`'s team.
func (g *Game) PlayerTeam(player *Player) *Team {
	if g.Home.IsInTeam(player) {
		return g.Home
	}
	return g.Away
}

// PlayerOpponentTeam returns `player`'s opponent team.
func (g *Game) PlayerOpponentTeam(player *Player) *Team {
	if g.Home.IsInTeam(player) {
		return g.Away
	}
	return g.Home
}

// OtherTeam return opponen team of `team`.
func (g *Game) OtherTeam(team *Team) *Team {
	if team == g.Home {
		return g.Away
	}
	return g.Home
}
