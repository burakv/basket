package game

import (
	"math/rand"
)

// A Team represents a team in a Basketball Game.
type Team struct {
	Players     []*Player
	Score       int
	AttackCount int
}

// NewTeam returns a team with 5 random players.
func NewTeam() *Team {
	players := make([]*Player, 5)
	for i := 0; i < 5; i++ {
		players[i] = NewPlayer(i + 1)
	}
	return &Team{Players: players}
}

// IsInTeam returns whether given `player` is in the `t`.
func (t *Team) IsInTeam(player *Player) bool {
	for _, p := range t.Players {
		if p == player {
			return true
		}
	}
	return false
}

// RandomPlayer returns a random player from `t`.
func (t *Team) RandomPlayer() *Player {
	return t.Players[rand.Intn(len(t.Players))]
}
