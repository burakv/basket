package simulation

import (
	"basket/game"
)

// A Start is a state that gives ball the home or away team with equal probability.
type Start struct{}

// Process processes given `game` for Start state.
func (s *Start) Process(game *game.Game) {
}

// Transitions returns possible next states.
func (s *Start) Transitions(game *game.Game) map[State]float64 {
	transitions := make(map[State]float64)
	transitions[&HoldBall{game.Home.RandomPlayer()}] = 0.5
	transitions[&HoldBall{game.Away.RandomPlayer()}] = 0.5
	return transitions
}
