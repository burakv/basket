package simulation

import (
	"basket/game"
)

const (
	twoPointShootProb   = 0.45
	threePointShootProb = 0.25
	instantShootProb    = 0.01
	keepBallProb        = 0.30
	passTeamMateProb    = 0.05
	stealBallProb       = 0.025
)

var _ State = &Attack{}

// An Attack is a state that picks shoot, pass or let's opponent to steal the ball.
type Attack struct{}

// Process processes given `game` for Attack state.
func (a *Attack) Process(game *game.Game) {
}

// Transitions returns possible next states.
func (a *Attack) Transitions(g *game.Game) map[State]float64 {
	transitions := make(map[State]float64)

	remainingTime := g.RemainingAttackDurationRatio()

	shootProb := instantShootProb
	if remainingTime <= 0.7 {
		shootProb = 1 - remainingTime
	}
	transitions[&Shoot{score: game.TwoPoint}] = shootProb * twoPointShootProb
	transitions[&Shoot{score: game.ThreePoint}] = shootProb * threePointShootProb

	owner := g.Ball.Owner
	for _, p := range g.PlayerTeam(owner).Players {
		if p != owner {
			transitions[&HoldBall{p}] = remainingTime * passTeamMateProb
		} else {
			transitions[&HoldBall{p}] = (1 - shootProb) * keepBallProb
		}
	}

	for _, p := range g.PlayerOpponentTeam(owner).Players {
		transitions[&HoldBall{p}] = stealBallProb
	}

	return transitions
}
