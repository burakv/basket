package simulation

import (
	"math/rand"

	"basket/game"
)

const (
	twoPointScoreChance   = 0.7
	threePointScoreChance = 0.5
)

// A Shoot is a state that attemps two or three point score with some chances.
type Shoot struct {
	score  int
	scored bool
}

// Process processes given `game` for Shoot state.
func (s *Shoot) Process(g *game.Game) {
	score := s.score
	chance := twoPointScoreChance
	if score == game.ThreePoint {
		chance = threePointScoreChance
	}

	scored := rand.Float64() < chance
	if scored {
		g.Scored(score)
	} else {
		g.Missed(score)
	}
}

// Transitions returns possible next states.
func (s *Shoot) Transitions(game *game.Game) map[State]float64 {
	transitions := make(map[State]float64)

	shooter := game.Ball.Owner

	if s.scored {
		transitions[&HoldBall{game.PlayerOpponentTeam(shooter).RandomPlayer()}] = 1.0
	} else {
		transitions[&HoldBall{game.PlayerOpponentTeam(shooter).RandomPlayer()}] = 0.7
		transitions[&HoldBall{game.PlayerTeam(shooter).RandomPlayer()}] = 0.3
	}

	return transitions
}
