package simulation_test

import (
	"testing"

	"basket/game"
	"basket/game/simulation"
)

func BenchmarkSimulation(b *testing.B) {
	g := game.NewGame()
	for i := 0; i < b.N; i++ {
		simulation.Simulate(g)
	}
}
