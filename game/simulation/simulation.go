// Package simulation provides utilities for simulating Basketball Game.
package simulation

import (
	"math/rand"
	"time"

	"basket/game"
)

// Simulate simulates given `game` until end.
func Simulate(game *game.Game) {
	var state State
	state = &Start{}

	for {
		game.Tick(1 * time.Second)

		state.Process(game)
		state = pickNextState(game, state)

		if game.End() {
			break
		}
	}
}

func pickNextState(game *game.Game, state State) State {
	transitions := state.Transitions(game)
	states := []State{}
	probs := []float64{}
	for state, prob := range transitions {
		states = append(states, state)
		probs = append(probs, prob)
	}
	return states[weightedRandom(probs)]
}

func weightedRandom(probs []float64) int {
	total := float64(0)
	for _, prob := range probs {
		total += prob
	}

	selected := randFloat64N(total)

	total = float64(0)
	for i, prob := range probs {
		total += prob
		if total >= selected {
			return i
		}
	}

	panic("unreachable")
}

func randFloat64N(n float64) float64 {
	return rand.Float64() * n
}
