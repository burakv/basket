package simulation

import (
	"basket/game"
)

// A HoldBall is a state that changes owner of the ball and
// resets the attack duration if ball is stolen.
type HoldBall struct {
	player *game.Player
}

// Process processes given `game` for HoldBall state.
func (hb *HoldBall) Process(game *game.Game) {
	game.HoldBall(hb.player)

	if game.Ball.PrewOwner != nil &&
		game.PlayerTeam(game.Ball.PrewOwner) != game.PlayerTeam(game.Ball.Owner) {
		game.ResetAttackDuration()
	}
}

// Transitions returns possible next states.
func (hb *HoldBall) Transitions(game *game.Game) map[State]float64 {
	transitions := make(map[State]float64)
	transitions[&Attack{}] = 1.0
	return transitions
}
