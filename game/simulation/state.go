package simulation

import "basket/game"

// A State represents a state in a Basketball Game.
type State interface {
	Process(game *game.Game)
	Transitions(game *game.Game) map[State]float64
}
