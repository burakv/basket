package simulation

import "basket/game"

// A Move is a state that moves ball to given `area`.
type Move struct {
	area game.Area
}

// Process processes given `game` for Move state.
func (m *Move) Process(game *game.Game) {
	game.Ball.Area = m.area
}

// Transitions returns possible next states.
func (m *Move) Transitions(game *game.Game) map[State]float64 {
	transitions := make(map[State]float64)
	transitions[&Attack{}] = 1.0
	return transitions
}
