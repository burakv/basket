package game

// An Area represents a partition of a Basketball Stadium.
type Area int

// Possible Area types.
const (
	AreaMiddle Area = iota
	AreaHomeTwoPoints
	AreaHomeThreePoints
	AreaAwayTwoPoints
	AreaAwayThreePoints
)
