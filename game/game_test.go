package game_test

import (
	"testing"
	"time"

	"basket/game"

	"github.com/stretchr/testify/require"
)

func TestCreatingGame(t *testing.T) {
	g := game.NewGame()

	require.NotNil(t, g)
	require.Nil(t, g.Ball.Owner)
	require.Len(t, g.Home.Players, 5)
	require.Len(t, g.Away.Players, 5)
}

func TestTickingGame(t *testing.T) {
	g := game.NewGame()

	require.Equal(t, g.Remaining, game.GameDuration)
	require.Equal(t, g.RemainingAttackTime, game.AttackDuration)

	delta := 10 * time.Second
	g.Tick(delta)

	require.Equal(t, g.Remaining, game.GameDuration-delta)
	require.Equal(t, g.RemainingAttackTime, game.AttackDuration-delta)

	g.HoldBall(g.Home.RandomPlayer())
	g.ResetAttackDuration()
	require.Equal(t, g.Remaining, game.GameDuration-delta)
	require.Equal(t, g.RemainingAttackTime, game.AttackDuration)
	require.Equal(t, g.RemainingAttackDurationRatio(), 1.0)
	require.Equal(t, g.Home.AttackCount, 1)
}

func TestGameEnd(t *testing.T) {
	g := game.NewGame()

	require.False(t, g.End())

	g.Tick(game.GameDuration)

	require.True(t, g.End())
}

func TestChangeBallHolder(t *testing.T) {
	g := game.NewGame()

	require.Nil(t, g.Ball.Owner)
	require.Nil(t, g.Ball.PrewOwner)

	playerA := g.Home.RandomPlayer()
	g.HoldBall(playerA)

	require.Equal(t, playerA, g.Ball.Owner)
	require.Nil(t, g.Ball.PrewOwner)

	playerB := g.Away.RandomPlayer()
	g.HoldBall(playerB)

	require.Equal(t, playerB, g.Ball.Owner)
	require.Equal(t, playerA, g.Ball.PrewOwner)
}

func TestGettingTeams(t *testing.T) {
	g := game.NewGame()

	playerA := g.Home.RandomPlayer()
	playerB := g.Away.RandomPlayer()
	require.Equal(t, g.PlayerTeam(playerA), g.Home)
	require.Equal(t, g.PlayerOpponentTeam(playerA), g.Away)
	require.Equal(t, g.PlayerTeam(playerB), g.Away)
	require.Equal(t, g.PlayerOpponentTeam(playerB), g.Home)

	require.Equal(t, g.OtherTeam(g.Home), g.Away)
	require.Equal(t, g.OtherTeam(g.Away), g.Home)
}

func TestScoring(t *testing.T) {
	g := game.NewGame()

	playerA := g.Home.Players[0]
	playerB := g.Home.Players[1]
	g.HoldBall(playerA)
	g.HoldBall(playerB)

	g.Scored(game.TwoPoint)

	require.Equal(t, 2, g.Home.Score)
	require.Equal(t, 1, playerB.TwoPointAttempts)
	require.Equal(t, 1, playerB.TwoPointScores)
	require.Equal(t, 1, playerA.Assists)

	g.HoldBall(playerA)
	g.Scored(game.ThreePoint)

	require.Equal(t, 5, g.Home.Score)
	require.Equal(t, 1, playerA.ThreePointAttempts)
	require.Equal(t, 1, playerA.ThreePointScores)
	require.Equal(t, 1, playerB.Assists)

	g.Missed(game.ThreePoint)

	require.Equal(t, 5, g.Home.Score)
	require.Equal(t, 2, playerA.ThreePointAttempts)
	require.Equal(t, 1, playerA.ThreePointScores)
	require.Equal(t, 1, playerB.Assists)

	g.Missed(game.TwoPoint)

	require.Equal(t, 5, g.Home.Score)
	require.Equal(t, 2, playerA.ThreePointAttempts)
	require.Equal(t, 1, playerA.ThreePointScores)
	require.Equal(t, 1, playerA.TwoPointAttempts)
	require.Equal(t, 0, playerA.TwoPointScores)
	require.Equal(t, 1, playerB.Assists)
}
